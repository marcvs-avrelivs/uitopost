#!/usr/bin/env node
'use strict';
const puppeteer = require('puppeteer');
const auth = require('./credentials.js'); // login credentials
const PushBullet = require('pushbullet'); // pushbullet for sharing photos
const fs = require('fs'); // readdir, access, etc
const Redis = require('redis'); // pub/sub rubadubdub
const pb = new PushBullet(auth.pbKey);


/**
* uitopost main class idk
* better dox l8r
*/
class UitoPoster {
  #timeouts = [];
  #browser = undefined;
  /**
  * constructor for uitoposter
  * @param {string} targetPage - target page
  * @param {Object} opts - options for uitoposter
  */
  constructor(targetPage, opts) {
    if (typeof opts.postID === 'undefined') {
      throw new Error('No post ID given');
    } else {
      this.postID = opts.postID;
    }
    this.pages = null;
    this.sel = { // css selectors
      login: { // selectors for login form
        user: 'input[name="email"]',
        pass: 'input[name="pass"]',
        submit: 'form[action*="login"] [type="submit"]',
      },
      post: { // selectors for page elements
        share: 'a._2nj7', // selector for share link
        shareToGroup: 'div > ul > li._54ni._3gc_'+
        '.__MenuItem > a', // selector for Share to a Group
        groupName: 'input[placeholder="Group name"]', // group name input
        postButton: 'button._2g61.selected._42ft', // post button
        includeOrig: 'div > div > div._3-9a '+
        '> label > input[type=checkbox]', // include original post option
        shareDialog: 'div._59s7 > div._4t2a',
        actorSelect: 'span[data-testid="actor-selector"] a > span._55pe',
        voiceSelect: 'div[data-testid="voice-selector-dropdown"] input._5r_g',
        targetVoice: 'div[data-testid="voice-selector-dropdown"] div[role="menuitem"]',

      },
    };
    if (!opts.browser) {
      throw new Error('Please pass a browser instance!');
    }
    this.#browser = opts.browser;
    this.headless = typeof opts.headless === 'undefined' ? true : opts.headless;
    this.pathPrefix = typeof opts.pathPrefix !== 'undefined' ?
                      opts.pathPrefix : '/var/node/uitopost';
    const baseDir = this.pathPrefix;
    const PAGE = targetPage;
    const configs = fs.readdirSync(`${baseDir}/pageData`);
    /* const confStr = configs.map((e) => {
      return e.replace(/\.js$/i, '');
    }).join(', ');*/
    if (configs.includes(`${PAGE}.js`)) {
      this.targetPage = require(`${baseDir}/pageData/${PAGE}.js`).facebook;
    } else {
      throw new Error(`Target page ${PAGE} not found.`);
    }
  }
  /**
  * launch browser
  */
  /*
  async launchBrowser() {
    console.log('launching browser');
    this.#browser = await puppeteer.launch({
      headless: this.headless,
      userDataDir: this.pathPrefix+'/data',
      args: [
        '--no-sandbox',
      ],
      slowMo: 200,
    });
    const context = this.#browser.defaultBrowserContext();
    await context.overridePermissions('https://www.facebook.com', ['notifications']);
    await this.openPage();
  }
  */
  /**
  * get page
  */
  async openPage() {
    const page = await this.#browser.newPage();
    await this.evadeHeadlessDetection(page);
    page.setViewport({width: 1200, height: 800, deviceScaleFactor: 1});
    this.page = page;
    try {
      await this.fblogin(page);
    } catch (e) { // already logged in
      console.log('already logged in');
    } finally {
      // get posts for target group
    }
  }
  /**
  * close browser
  */
  /* async closeBrowser() {
    console.log('closing browser');
    if(this.#browser) {
      try {
        const pages = this.#browser.pages();
        for(var idx in pages) {
          page = pages[idx];
          await page.goto('about:blank');
          await page.close();
          pages[idx] = null;
        }
        await this.#browser.close();
      } catch (e) {
        console.log(' error closing browser: ');
        console.error(e);
      } finally {
        this.#browser = null;
      }
    }
  }*/
  /**
   * run puppeteer!
   */
  async run() {
    await this.openPage();
    await this.getPost(this.postID, true);
  };
  /**
   * Coordinates for screenshot
   * @typedef {Object} Coordinates
   * @property {int} x - left edge of element
   * @property (int) y - top edge of element
   * @property {number} width - width of element
   * @property {number} height - height of element
   */

  /**
   * Options for screenshot
   * @typedef {Object} ScreenshotOpts
   * @property {string=} filePrefix - prefix for filename
   * @property {string=} filePath - path to save file
   * @property {Coordinates=} coords - optional {@link Coordinates} to target
   */


  /**
   * capture screenshot of a given page
   * @param {Page} page - an active puppeteer page
   * @param {ScreenshotOpts=} opts - optional arguments
   */
  async screenshot(page, opts) {
    const defaults = {
      filePrefix: 'fb',
      filePath: '/tmp',
    };
    opts = Object.assign(defaults, opts);
    console.dir(opts);
    // const target = null;
    const nonce = typeof opts.nonce === 'undefined' ?
      '-'+Date.now() : opts.nonce;
    const filename = `${opts.filePrefix}${nonce}.png`; // filename
    const path = `${opts.filePath}/${filename}`;
    const title = typeof opts.title === 'undefined' ?
                  opts.filePrefix :
                  opts.title;
    const screenshotOpts = {
      path,
    };
    // if (typeof opts.element === 'undefined') {
    //  console.log('did not receive element object');

    if (typeof opts.coords === 'undefined' || isEmptyObject(opts.coords)) {
      console.log('capturing full page');
      screenshotOpts.fullPage = true;
    } else {
      console.log('capturing clipped area');
      screenshotOpts.clip = opts.coords;
    }

    //  target = page;
    // } else {
    //  console.log('capturing specific element');
    //  target = opts.element;
    // }

    console.log('capturing screenshot to '+path);
    await page.screenshot(screenshotOpts);

    console.log('pushing screenshot to pushbullet');
    pb.file({}, path, title, (e, r) => {
      if (e) {
        console.error('Error pushing photo: ', e);
      } else {
        console.log(title, 'pushed to pushbullet successfully');
      }
    });
  }

  /**
   * check if object is empty
   * @param {Object} obj - object to check
   * @return {boolean} true if object is empty
   */
  isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }

  /**
   * login to facebook
   * @param {Page} page - active puppeteer page
   */
  async fblogin() {
    // navigate to fb
    const page = this.page;
    await page.goto('https://facebook.com', {waitUntil: 'domcontentloaded'});
    console.log('attempting fb login');
    try {
      await page.waitForSelector(this.sel.login.submit, {timeout: 2000});
    } catch (e) {
      throw e;
    }
    // focus username field and type it
    page = this.page;
    console.log('inputting username');
    await page.focus(this.sel.login.user);
    await page.keyboard.type(auth.username);

    // password field
    console.log('inputting password');
    await page.focus(this.sel.login.pass);
    await page.keyboard.type(auth.password);

    // click login
    console.log('submitting login form');
    await page.click(this.sel.login.submit);
    await page.waitForNavigation();
    console.log('Logged In!');
  }

  /**
   * get posts from post id
   * @param {Page} page - active puppeteer page
   * @param {number} postID - id of post on facebook
   * @param {boolean=} schedule - whether or not to schedule share(s)
   */
  async getPost(postID, schedule) {
    const page = this.page;
    console.log(`getting post ${postID}`);
    const slug = this.targetPage.slug;
    const url = `https://facebook.com/${slug}/posts/${postID}`;
    const HOUR = 1000*60*60;
    await page.goto(url, {waitUntil: 'domcontentloaded'});
    if (typeof schedule !== 'undefined' && schedule) {
      for (const idx in this.targetPage.groups) {
        const group = this.targetPage.groups[idx];
        const OFFSET = HOUR*idx;
        console.log(`Scheduling ${postID} share to ${group} with time offset: ${OFFSET}`);
        this.#timeouts.push({
          group,
          timeout: setTimeout(async (postID, group) => {
            await this.sharePost(postID, group);
          }, OFFSET, postID, group),
        });
        // await sharePost(page, group);
      }
      await page.goto('about:blank');
      // await page.close();
      // page = null;
    }
  }
  /* async getPagePosts(page, slug) {
    // navigate to CarlisleYP page
    const url = `https://facebook.com/${slug}/posts`;
    console.log(url);
    await page.goto(url, {waitUntil: 'domcontentloaded'});
    console.log('navigated to'+url);

    await page.waitForSelector(this.sel.post.post);
    const posts = await page.evaluate((selectors, needle) => {
      const posts = Array.from(
          document.querySelectorAll(selectors.post),
      );
      const gd = posts.filter((e) => e.innerText.
          match(new RegExp(needle, 'i'),
          ));
      gd.map((e) => {
        e.classList.add('uito-target');
      });
      return gd.map((e) => {
        const {x, y, width, height} = e.getBoundingClientRect();
        return {x, y, width, height};
      });
      document.querySelector('.uito-target').scrollIntoView();
      document.querySelector(
          '.uito-target '+selectors.share,
      ).style.backgroundColor='black !important';
      return gd.map((e) => e.innerHTML);
    }, this.sel.post, this.targetPage.needle);
    if (posts.length > 0) {
      // await sharePost(page, this.targetPage.groups[0]);
      for (const group of this.targetPage.groups) {
        await sharePost(page, group);
      }
    } else {
      console.log('no posts found');
    }
    return posts;
  }*/

  /**
   * configure a given page object to try and
   * subvert headless detection
   * @param {Page} page - active puppeteer page
   */
  async evadeHeadlessDetection(page) {
    const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
    'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
    await page.setUserAgent(userAgent);
    await page.evaluateOnNewDocument(() => {
      Object.defineProperty(navigator, 'webdriver', {
        get: () => false,
      });
      window.navigator.chrome = {
        app: {
          isInstalled: false,
        },
        webstore: {
          onInstallStageChanged: {},
          onDownloadProgress: {},
        },
        runtime: {
          PlatformOs: {
            MAC: 'mac',
            WIN: 'win',
            ANDROID: 'android',
            CROS: 'cros',
            LINUX: 'linux',
            OPENBSD: 'openbsd',
          },
          PlatformArch: {
            ARM: 'arm',
            X86_32: 'x86-32',
            X86_64: 'x86-64',
          },
          PlatformNaclArch: {
            ARM: 'arm',
            X86_32: 'x86-32',
            X86_64: 'x86-64',
          },
          RequestUpdateCheckStatus: {
            THROTTLED: 'throttled',
            NO_UPDATE: 'no_update',
            UPDATE_AVAILABLE: 'update_available',
          },
          OnInstalledReason: {
            INSTALL: 'install',
            UPDATE: 'update',
            CHROME_UPDATE: 'chrome_update',
            SHARED_MODULE_UPDATE: 'shared_module_update',
          },
          OnRestartRequiredReason: {
            APP_UPDATE: 'app_update',
            OS_UPDATE: 'os_update',
            PERIODIC: 'periodic',
          },
        },
      };
      const originalQuery = window.navigator.permissions.query;
      return window.navigator.permissions.query = (parameters) => (
      parameters.name === 'notifications' ?
        Promise.resolve({state: Notification.permission}) :
        originalQuery(parameters)
      );
      Object.defineProperty(navigator, 'plugins', {
        get: () => 'oof'.split(''),
      });
      Object.defineProperty(navigator, 'languages', {
        get: () => ['en-US', 'en'],
      });
    });
  }

  /**
   * share post to specific group
   * @param {number} postID - ID of target post
   * @param {string} group - name of group to share to
   */
  async sharePost(postID, group) {
    console.log(`in sharePost(${postID}, '${group}')`);
    if (!this.#browser) {
      console.log('lol browser ded');
      page = await this.launchBrowser();
    }
    if (!this.page) {
      await this.openPage();
    }
    await this.getPost(postID);
    const page = this.page;
    try {
      console.log('clicking share link');
      await page.waitForSelector(this.sel.post.share, {timeout: 2000});
      await page.click(this.sel.post.share);
      await page.waitForSelector(this.sel.post.shareToGroup, {timeout: 5000});
      console.log('clicking share to group');
      await page.click(this.sel.post.shareToGroup);
      await page.waitForSelector(this.sel.post.groupName, {timeout: 5000});
      const isSelf = await page.evaluate((sel) => {
        const actor = document.querySelector(sel.actorSelect);
        return actor.innerText.toLowerCase() === 'gary warman';
      }, this.sel.post);
      if (isSelf) {
        try {
          console.log('changing post as to proper group');
          await page.click(this.sel.post.actorSelect);
          await page.waitForSelector(this.sel.post.voiceSelect, {timeout: 3000});
          await page.focus(this.sel.post.voiceSelect);
          await page.keyboard.type(this.targetPage.actorQuery);
          if (await page.evaluate(
              (sel, q) => {
                const target = document.querySelector(sel);
                return target.innerText.match(new RegExp(q, 'i'));
              },
              this.sel.post.targetVoice, this.targetPage.actorQuery,
          )) {
            console.log('selecting page as target voice');
            await page.click(this.sel.post.targetVoice);
          }
        } catch(e) {
          console.log('unable to change post as uwu');
          return
        }
      }
      console.log('clicking include original');
      await page.click(this.sel.post.includeOrig);
      console.log('focusing on group name');
      await page.focus(this.sel.post.groupName);
      await page.keyboard.type(group);
      console.log('typed group name');
      await page.waitFor(1000);
      await page.keyboard.press('ArrowDown');
      console.log('arrow down!');
      await page.waitFor(1000);
      await page.keyboard.press('Enter');
      console.log('enter pressed');
      await page.waitFor(500);
      const postButton = await page.$(this.sel.post.postButton);
      console.log('clickin post butan!');
      await postButton.click();
      await page.waitFor(250);
      try {
        await page.click(this.sel.post.shareDialog);
      } catch (e) {
        console.log(`Post ${postID} shared successfully to ${group}`);
      }
    } catch (e) {
      console.log('error sharing post to', group, ': ');
      console.error(e);
    } finally {
      await page.goto('about:blank');
      // await page.close();
      // page = null;
      // await this.closeBrowser();
    }
  }
}

(async () => {
  const client = Redis.createClient();
  const args = process.argv.slice(2);
  const headless = !args.length;
  const pathPrefix = __dirname;
  console.log('Headless:', headless);
  console.log('Launching browser...');
  const browser = await puppeteer.launch({
    headless,
    userDataDir: pathPrefix+'/data',
    args: [
      '--no-sandbox',
      '--process-per-site',
    ],
    slowMo: 200,
  });
  console.log('Browser launched!');
  const context = browser.defaultBrowserContext();
  await context.overridePermissions('https://www.facebook.com', ['notifications']);

  client.on('message', (channel, msg) => {
    const json = JSON.parse(msg);
    const {postID, targetPage} = json;
    console.log(`message received from ${channel}`);
    console.dir(json);
    // try {
    const poster = new UitoPoster(targetPage, {
      headless,
      postID,
      browser,
    });
    poster.run();
    // } catch (e) {
    //  console.error(`Error posting ${postID} to ${targetPage}: ${e}`);
    // }
  });

  client.subscribe('uitopost');
})();
