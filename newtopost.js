#!/usr/bin/env node
'use strict';
const puppeteer = require('puppeteer');
const fs = require('fs'); // readdir, access, etc
const Redis = require('redis'); // pub/sub rubadubdub
const {FacebookSharer} = require('./lib/FacebookSharer.js');
const {LinkedInSharer} = require('./lib/LinkedInSharer.js');


/**
* uitopost main class idk
* better dox l8r
*/
class UitoPoster {
  #browser = undefined;
  workers = {};
  /**
  * constructor for uitoposter
  * @param {string} targetPage - target page
  * @param {Object} opts - options for uitoposter
  */
  constructor(opts) {
    this.pages = null;
    if (!opts.browser) {
      throw new Error('Please pass a browser instance!');
    }
    this.#browser = opts.browser;
    this.headless = typeof opts.headless === 'undefined' ? true : opts.headless;
    this.pathPrefix = typeof opts.pathPrefix !== 'undefined' ?
                      opts.pathPrefix : '/var/node/uitopost';
    const baseDir = this.pathPrefix;
    const configs = fs.readdirSync(`${baseDir}/pageData`);
    /* const confStr = configs.map((e) => {
      return e.replace(/\.js$/i, '');
    }).join(', ');*/
  }
  /**
   * check if worker exists
   * @param {string} worker - worker name (page name)
   * @return {boolean}
   */
  hasWorker(worker) {
    return typeof this.workers[worker] !== 'undefined';
  }
  /**
   * create worker
   * @param {string} worker - worker name
   */
  createWorker(worker) {
    this.workers[worker] = {};
  }
  /**
   * trigger worker for given network
   * @param {string} targetPage - target page
   * @param {opts} opts - options
   *    (postID: post id, network: [facebook|linkedin])
   */
  triggerWorker(opts) {
    const {targetPage, postID, network} = opts;
    console.log('triggering worker for',network+'::'+targetPage,'(postID:',postID+')');
    if (!this.workers[targetPage][network]) {
      switch (network) {
        case 'facebook':
          this.workers[targetPage][network] =
            new FacebookSharer({
              page: targetPage,
              browser: this.#browser,
              origin: this,
            });
          break;
        case 'linkedin':
          this.workers[targetPage][network] =
            new LinkedInSharer({
              page: targetPage,
              browser: this.#browser,
              origin: this,
            });
          break;
      }
    }
    this.workers[targetPage][network].run(postID);
  }
  /**
  * launch browser
  */
  async launchBrowser() {
    console.log('launching browser');
    this.#browser = await puppeteer.launch({
      headless: this.headless,
      userDataDir: this.pathPrefix+'/data',
      args: [
        '--no-sandbox',
      ],
      slowMo: 200,
    });
    const context = this.#browser.defaultBrowserContext();
    await context.overridePermissions('https://www.facebook.com', ['notifications']);
    await this.openPage();
  }
  /**
   * configure a given page object to try and
   * subvert headless detection
   * @param {Page} page - active puppeteer page
   */
  async evadeHeadlessDetection(page) {
    const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
    'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
    await page.setUserAgent(userAgent);
    await page.evaluateOnNewDocument(() => {
      Object.defineProperty(navigator, 'webdriver', {
        get: () => false,
      });
      window.navigator.chrome = {
        app: {
          isInstalled: false,
        },
        webstore: {
          onInstallStageChanged: {},
          onDownloadProgress: {},
        },
        runtime: {
          PlatformOs: {
            MAC: 'mac',
            WIN: 'win',
            ANDROID: 'android',
            CROS: 'cros',
            LINUX: 'linux',
            OPENBSD: 'openbsd',
          },
          PlatformArch: {
            ARM: 'arm',
            X86_32: 'x86-32',
            X86_64: 'x86-64',
          },
          PlatformNaclArch: {
            ARM: 'arm',
            X86_32: 'x86-32',
            X86_64: 'x86-64',
          },
          RequestUpdateCheckStatus: {
            THROTTLED: 'throttled',
            NO_UPDATE: 'no_update',
            UPDATE_AVAILABLE: 'update_available',
          },
          OnInstalledReason: {
            INSTALL: 'install',
            UPDATE: 'update',
            CHROME_UPDATE: 'chrome_update',
            SHARED_MODULE_UPDATE: 'shared_module_update',
          },
          OnRestartRequiredReason: {
            APP_UPDATE: 'app_update',
            OS_UPDATE: 'os_update',
            PERIODIC: 'periodic',
          },
        },
      };
      const originalQuery = window.navigator.permissions.query;
      return window.navigator.permissions.query = (parameters) => (
      parameters.name === 'notifications' ?
        Promise.resolve({state: Notification.permission}) :
        originalQuery(parameters)
      );
      Object.defineProperty(navigator, 'plugins', {
        get: () => 'oof'.split(''),
      });
      Object.defineProperty(navigator, 'languages', {
        get: () => ['en-US', 'en'],
      });
    });
  }
}

(async () => {
  const client = Redis.createClient();
  const args = process.argv.slice(2);
  const headless = !args.length;
  const pathPrefix = __dirname;
  console.log('Headless:', headless);
  console.log('Launching browser...');
  const browser = await puppeteer.launch({
    headless,
    userDataDir: pathPrefix+'/data',
    args: [
      '--no-sandbox',
      '--process-per-site',
    ],
    slowMo: 200,
  });
  console.log('Browser launched!');
  const context = browser.defaultBrowserContext();
  await context.overridePermissions('https://www.facebook.com', ['notifications']);
  await context.overridePermissions('https://www.linkedin.com', ['notifications']);
  const Uito = new UitoPoster({headless, browser});
  client.on('message', (channel, msg) => {
    const json = JSON.parse(msg);
    let {postID, targetPage, network} = json;
    if ( !network || !['facebook', 'linkedin'].includes(network) ) {
      network = 'facebook';
    }
    console.log(`message received from ${channel}`);
    console.dir(json);
    // try {
    if (!Uito.hasWorker(targetPage)) {
      Uito.createWorker(targetPage);
    }
    Uito.triggerWorker({
      targetPage,
      postID,
      network,
    },
    );
    // } catch (e) {
    //  console.error(`Error posting ${postID} to ${targetPage}: ${e}`);
    // }
  });

  client.subscribe('uitopost');
})();
