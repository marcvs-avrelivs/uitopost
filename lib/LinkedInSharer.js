'use strict';
const fs = require('fs'); // readdir, access, etc


/**
* linkedin sharer main class idk
* better dox l8r
*/
class LinkedInSharer {
  #timeouts = [];
  #browser = undefined;
  #auth = {};
  /**
  * constructor for uitoposter
  * @param {string} targetPage - target page
  * @param {Object} opts - options for uitoposter
  */
  constructor(opts) {
    this.page = null;
    const targetPage = opts.page;
    this.sel = { // css selectors
      login: { // selectors for login form
        user: 'input#username',
        pass: 'input#password',
        submit: 'form.login_form button[type="submit"]',
      },
      post: { // selectors for page elements
        share: '.share-reshare-dropdown button', // selector for share link
        shareVisibility: 'button'+
            '[data-control-name="share.visibility_button"]', // visibility list
        groupMembers: 'button'+
            '[data-control-name="share.open_GROUP_list"]', // group list
        groupList: 'ul.share-visibility-options__list', // post button
        groupButton: 'button'+
            '[data-control-name="share.update_GROUP_selection"]',
        groupDone: 'button'+
            '[data-control-name="share.GROUP_done"]',
        visibilityDone: 'button'+
            '[data-control-name="share.change_visibility_close"]',
        postButton: 'button'+
            '[data-control-name="share.post"]',
        toast: 'li[data-test-artdeco-toast-item-type="success"]',

      },
    };
    if (!opts.browser) {
      throw new Error('Please pass a browser instance!');
    }
    this.#browser = opts.browser;
    this.origin = opts.origin;
    this.headless = typeof opts.headless === 'undefined' ? true : opts.headless;
    this.pathPrefix = typeof opts.pathPrefix !== 'undefined' ?
                      opts.pathPrefix : '/var/node/uitopost';
    const baseDir = this.pathPrefix;
    const PAGE = targetPage;
    this.usr = targetPage;
    this.#auth = require(`${baseDir}/credentials.js`).linkedin;
    const configs = fs.readdirSync(`${baseDir}/pageData`);
    /* const confStr = configs.map((e) => {
      return e.replace(/\.js$/i, '');
    }).join(', ');*/
    if (configs.includes(`${PAGE}.js`)) {
      this.targetPage = require(`${baseDir}/pageData/${PAGE}.js`).linkedin;
    } else {
      throw new Error(`Target page ${PAGE} not found.`);
    }
  }
  /**
  * get page
  */
  async openPage() {
    const page = await this.#browser.newPage();
    await this.origin.evadeHeadlessDetection(page);
    page.setViewport({width: 1200, height: 800, deviceScaleFactor: 1});
    this.page = page;
    try {
      await this.login(page);
    } catch (e) { // already logged in
      console.log('already logged in');
    } finally {
      // oof
    }
  }
  /**
   * run puppeteer!
   * @param {string} postID - linkedin post id (urn:li:...)
   */
  async run(postID) {
    await this.openPage();
    await this.getPost(postID, true);
  };
  /**
   * Coordinates for screenshot
   * @typedef {Object} Coordinates
   * @property {int} x - left edge of element
   * @property (int) y - top edge of element
   * @property {number} width - width of element
   * @property {number} height - height of element
   */

  /**
   * Options for screenshot
   * @typedef {Object} ScreenshotOpts
   * @property {string=} filePrefix - prefix for filename
   * @property {string=} filePath - path to save file
   * @property {Coordinates=} coords - optional {@link Coordinates} to target
   */


  /**
   * login to in of links
   */
  async login() {
    page = this.page;
    // navigate to linkedin
    await page.goto('https://linkedin.com/login', {waitUntil: 'domcontentloaded'});
    try {
      await page.waitForSelector(this.sel.login.submit, {timeout: 2000});
    } catch (e) {
      throw e;
    }
    console.log('attempting linkedin login');
    // focus username field and type it
    console.log('inputting username');
    await page.focus(this.sel.login.user);
    await page.keyboard.type(this.#auth.username);

    // password field
    console.log('inputting password');
    await page.focus(this.sel.login.pass);
    await page.keyboard.type(this.#auth.password);

    // click login
    console.log('submitting login form');
    await page.click(this.sel.login.submit);
    await page.waitForNavigation();
    console.log('Logged In!');
  }

  /**
   * get posts from post id
   * @param {string} postID - id of post on facebook
   * @param {boolean=} schedule - whether or not to schedule share(s)
   */
  async getPost(postID, schedule) {
    const page = this.page;
    console.log(`[linkedin::${this.usr}] getting post ${postID}`);
    //const slug = this.targetPage.slug;
    const url = `https://www.linkedin.com/feed/update/${postID}`;
    const HOUR = 1000*60*60;
    await page.goto(url, {waitUntil: 'domcontentloaded'});
    if (typeof schedule !== 'undefined' && schedule) {
      for (const idx in this.targetPage.groups) {
        const group = this.targetPage.groups[idx];
        const OFFSET = HOUR*idx;
        console.log(`[linkedin::${this.usr}] Scheduling ${postID} share `+
          `to ${group} with time offset: ${OFFSET}`);
        this.#timeouts.push({
          group,
          timeout: setTimeout(async (postID, group) => {
            await this.sharePost(postID, group);
          }, OFFSET, postID, group),
        });
        // await sharePost(page, group);
      }
      await page.goto('about:blank');
      // await page.close();
      // page = null;
    }
  }

  /**
   * share post to specific group
   * @param {number} postID - ID of target post
   * @param {string} group - name of group to share to
   */
  async sharePost(postID, group) {
    console.log(`[linkedin::${this.usr}] in sharePost(${postID}, '${group}')`);
    if (!this.page) {
      await this.openPage();
    }
    await this.getPost(postID);
    const page = this.page;
    try {
      console.log(`[linkedin::${this.usr}] clicking share button`);
      await this.clickwait(this.sel.post.share, 10000);
      console.log(`[linkedin::${this.usr}] clicking share visibility selector`);
      await this.clickwait(this.sel.post.shareVisibility, 5000);
      console.log(`[linkedin::${this.usr}] clicking share to group members`);
      await this.clickwait(this.sel.post.groupMembers, 5000);
      await this.page.waitForSelector(this.sel.post.groupList, {timeout: 5000});
      console.log(`[linkedin::${this.usr}] clicking group button`);
      const found = await page.evaluate((sel, group) => {
        function ere(string) {
            return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
        }
        const groups = document.querySelectorAll(sel);
        let ret = false;
        for (const e of groups) {
          if (e.innerText.match(new RegExp(ere(group), 'i'))) {
            e.click();
            ret = true;
          }
        }
        //return Array.from(groups).map(e => e.innerText);
        return ret;
      }, this.sel.post.groupButton, group);
      if (!found) {
        console.log(`[linkedin::${this.usr}] `+
          `group not found in list: ${group}`);
        return;
      }
      console.log(`[linkedin::${this.usr}] clicking groupDone`);
      await this.clickwait(this.sel.post.groupDone);
      console.log(`[linkedin::${this.usr}] clicking visibilityDone`);
      await this.clickwait(this.sel.post.visibilityDone);
      console.log(`[linkedin::${this.usr}] sharing post`);
      await this.clickwait(this.sel.post.postButton);
      await page.waitFor(250);
      try {
        await page.waitForSelector(this.sel.post.toast);
        console.log(`Post ${postID} shared successfully to ${group}`);
      } catch (e) {
        console.log(`[linkedin::${this.usr}] did not see `+
            `success toast for ${postID} in ${group}`);
      }
    } catch (e) {
      console.log(`[linkedin::${this.usr}] error in sharing post to ${group}:`);
      console.error(e);
    } finally {
      await page.goto('about:blank');
      // await page.close();
      // page = null;
      // await this.closeBrowser();
    }
  }
  /**
   * wait for an element then click it.
   * @param {string} selector - css selector for element
   * @param {number} timeout - how long to wait for
   */
  async clickwait(selector, timeout) {
    if (!timeout) {
      timeout = 30000;
    }
    await this.page.waitForSelector(selector, {timeout: timeout});
    await this.page.click(selector);
  }
}

module.exports.LinkedInSharer = LinkedInSharer;
