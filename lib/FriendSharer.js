'use strict';
const fs = require('fs'); // readdir, access, etc


/**
* uitopost main class idk
* better dox l8r
*/
class FriendSharer {
  #timeouts = [];
  #browser = undefined;
  #auth = {};
  /**
  * constructor for uitoposter
  * @param {string} targetPage - target page
  * @param {Object} opts - options for uitoposter
  */
  constructor(opts) {
    this.page = null;
    const targetPage = opts.page;
    this.usr = targetPage;
    this.sel = { // css selectors
      login: { // selectors for login form
        user: 'input[name="email"]',
        pass: 'input[name="pass"]',
        submit: 'form[action*="login"] [type="submit"]',
      },
      post: { // selectors for page elements
        voiceSelect: 'div._1dnh > div._1dnk._29ak > span > a', // voice selector (e.g. switch user context)
        voiceInput: 'div > div._4qlo._1t28 > input[tabindex=\'0\']', // input for voice selector
        share: 'a._2nj7', // selector for share link
        shareLink: 'div > ul > li._54ni._2al8.__MenuItem > a',
        shareToFriend: 'div > ul > li._54ni._1n80'+
        '.__MenuItem > a', // selector for Share to friend's page
        friendName: 'input[placeholder=\'friend\\\'s name\' i]', // friend name input
        postButton: 'button._2g61.selected._42ft', // post button
        includeOrig: '[aria-label=reshare i] ._4t2a input[type=checkbox]', // include original post
        shareDialog: '[aria-label=reshare i] ._4t2a',
        targetVoice: 'div[role=menuitem] ._2sl4',

      },
    };
    if (!opts.browser) {
      throw new Error('Please pass a browser instance!');
    }
    this.#browser = opts.browser;
    this.origin = opts.origin;
    this.headless = typeof opts.headless === 'undefined' ? true : opts.headless;
    this.pathPrefix = typeof opts.pathPrefix !== 'undefined' ?
                      opts.pathPrefix : '/var/node/uitopost';
    const baseDir = this.pathPrefix;
    this.#auth = require(`${baseDir}/credentials.js`).facebook;
    const PAGE = targetPage;
    const configs = fs.readdirSync(`${baseDir}/pageData`);
    /* const confStr = configs.map((e) => {
      return e.replace(/\.js$/i, '');
    }).join(', ');*/
    if (configs.includes(`${PAGE}.js`)) {
      const conf = require(`${baseDir}/pageData/${PAGE}.js`);
      this.friends = conf.friends;
      this.targetPage = conf.facebook;
    } else {
      throw new Error(`Target page ${PAGE} not found.`);
    }
  }
  /**
  * get page
  */
  async openPage() {
    const page = await this.#browser.newPage();
    await this.origin.evadeHeadlessDetection(page);
    page.setViewport({width: 1200, height: 800, deviceScaleFactor: 1});
    this.page = page;
    try {
      await this.login();
    } catch (e) { // already logged in
      console.log('already logged in');
    } finally {
      // oof
    }
  }
  /**
   * run puppeteer!
   * @param {number} postID - id of post to share
   */
  async run(postID) {
    await this.openPage();
    await this.getPost(postID, true);
  };
  /**
   * Coordinates for screenshot
   * @typedef {Object} Coordinates
   * @property {int} x - left edge of element
   * @property (int) y - top edge of element
   * @property {number} width - width of element
   * @property {number} height - height of element
   */

  /**
   * Options for screenshot
   * @typedef {Object} ScreenshotOpts
   * @property {string=} filePrefix - prefix for filename
   * @property {string=} filePath - path to save file
   * @property {Coordinates=} coords - optional {@link Coordinates} to target
   */


  /**
   * check if object is empty
   * @param {Object} obj - object to check
   * @return {boolean} true if object is empty
   */
  isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }

  /**
   * login to facebook
   * @param {Page} page - active puppeteer page
   */
  async login() {
    page = this.page;
    try {
      await page.waitForSelector(this.sel.login.submit, {timeout: 2000});
    } catch (e) {
      throw e;
    }
    // navigate to fb
    await page.goto('https://facebook.com', {waitUntil: 'domcontentloaded'});
    console.log('attempting fb login');
    // focus username field and type it
    console.log('inputting username');
    await page.focus(this.sel.login.user);
    await page.keyboard.type(this.#auth.username);

    // password field
    console.log('inputting password');
    await page.focus(this.sel.login.pass);
    await page.keyboard.type(this.#auth.password);

    // click login
    console.log('submitting login form');
    await page.click(this.sel.login.submit);
    await page.waitForNavigation();
    console.log('Logged In!');
  }

  /**
   * get posts from post id
   * @param {number} postID - id of post on facebook
   * @param {boolean=} schedule - whether or not to schedule share(s)
   */
  async getPost(postID, schedule) {
    const page = this.page;
    console.log(`[fbfriends::${this.usr}] getting post ${postID}`);
    const slug = this.targetPage.slug;
    const url = `https://facebook.com/${slug}/posts/${postID}`;
    const MINUTE = 1000*60;
    const HOUR = MINUTE*60;
    await page.goto(url, {waitUntil: 'domcontentloaded'});
    if (typeof schedule !== 'undefined' && schedule) {
      for (const idx in this.friends) {
        const friend = this.friends[idx];
        const OFFSET = (MINUTE*30) + (HOUR*idx);
        //const OFFSET = MINUTE*idx;
        console.log(`[fbfriends::${this.usr}] Scheduling ${postID} share to `+
                    `${friend} with time offset: ${OFFSET}`);
        this.#timeouts.push({
          friend,
          timeout: setTimeout(async (postID, friend) => {
            await this.sharePost(postID, friend);
          }, OFFSET, postID, friend),
        });
        // await sharePost(page, group);
      }
      await page.goto('about:blank');
      // await page.close();
      // page = null;
    }
  }

  /**
   * share post to specific group
   * @param {number} postID - ID of target post
   * @param {string} group - name of group to share to
   */
  async sharePost(postID, friend) {
    console.log(`[fbfriends::${this.usr}] in sharePost(${postID}, "${friend}")`);
    if (!this.page) {
      await this.openPage();
    }
    await this.getPost(postID);
    const page = this.page;
    try {
      await page.waitForSelector(this.sel.post.voiceSelect);
      await page.click(this.sel.post.voiceSelect);
      console.log(`[fbfriends::${this.usr}::${friend}] `+
        `changing post voice to self`);
      await page.waitForSelector(
          this.sel.post.voiceInput, {timeout: 3000},
      );
      await page.focus(this.sel.post.voiceInput);
      await page.keyboard.type('Gary Warman');
      if (await page.evaluate(
          (sel, q) => {
            const target = document.querySelector(sel);
            const ret = target.innerText.match(/gary warman/i);
            return ret;
          },
          this.sel.post.targetVoice,
      )) {
        console.log(`[fbfriends::${this.usr}::${friend}] `+
          `selecting page as target voice`);
        await page.click(this.sel.post.targetVoice);
      }
      console.log(`[fbfriends::${this.usr}::${friend}] clicking share link`);
      await page.waitForSelector(this.sel.post.share, {timeout: 2000});
      await page.click(this.sel.post.share);
      if (friend == 'self') {
        console.log(`[fbfriends::${this.usr}] clicking share to share to own page`);
        await page.click(this.sel.post.shareLink);
      } else {
        await page.waitForSelector(this.sel.post.shareToFriend, {timeout: 5000});
        console.log(`[fbfriends::${this.usr}::${friend}] clicking share to group`);
        await page.click(this.sel.post.shareToFriend);
        await page.waitForSelector(this.sel.post.friendName, {timeout: 5000});
        console.log(`[fbfriends::${this.usr}::${friend}] focusing on friend name`);
        await page.focus(this.sel.post.friendName);
        await page.keyboard.type(friend);
        console.log(`[fbfriends::${this.usr}::${friend}] typed ${friend}`);
        await page.waitFor(1000);
        await page.keyboard.press(`ArrowDown`);
        console.log(`[fbfriends::${this.usr}::${friend}] arrow down!`);
        await page.waitFor(1000);
        await page.keyboard.press(`Enter`);
        console.log(`[fbfriends::${this.usr}::${friend}] enter pressed`);
        await page.waitFor(500);
      }
      try {
        console.log(`[facebook::${this.usr}] clicking include original`);
        await page.click(this.sel.post.includeOrig);
      } catch (e) {
        console.log(`[facebook::${this.usr}] no original to include. skipping`);
      }
      const postButton = await page.$(this.sel.post.postButton);
      console.log(`[fbfriends::${this.usr}::${friend}] clickin post butan!`);
      await postButton.click();
      await page.waitFor(250);
      let success;
      try {
        await page.waitForSelector('._4t2a ._pig', {timeout: 5000});
        const res = await page.evaluate(() => {
          return document.querySelector('._4t2a ._pig').innerText;
        });
        success = res.match(/shared/i);
      } catch (e) {
      }
      if (success) {
        console.log(`[fbfriends::${this.usr}::${friend}] Post ${postID} `+
            `shared successfully to ${friend}`);
      } else {
        console.log(`[fbfriends::${this.usr}::${friend}] Post ${postID} `+
            `unable to share to ${friend}`);
      }
    } catch (e) {
      console.log(`[fbfriends::${this.usr}::${friend}] error sharing post to`, friend, `: `);
      console.error(e);
    } finally {
      await page.goto('about:blank');
      // await page.close();
      // page = null;
      // await this.closeBrowser();
    }
  }
}

module.exports.FriendSharer = FriendSharer;
