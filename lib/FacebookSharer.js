'use strict';
const fs = require('fs'); // readdir, access, etc
const {FriendSharer} = require('./FriendSharer.js');


/**
* uitopost main class idk
* better dox l8r
*/
class FacebookSharer {
  #timeouts = [];
  #browser = undefined;
  #auth = {};
  /**
  * constructor for uitoposter
  * @param {string} targetPage - target page
  * @param {Object} opts - options for uitoposter
  */
  constructor(opts) {
    this.page = null;
    const targetPage = opts.page;
    this.usr = targetPage;
    this.sel = { // css selectors
      login: { // selectors for login form
        user: 'input[name="email"]',
        pass: 'input[name="pass"]',
        submit: 'form[action*="login"] [type="submit"]',
      },
      post: { // selectors for page elements
        share: 'a._2nj7', // selector for share link
        shareToGroup: 'div > ul > li._54ni._3gc_'+
        '.__MenuItem > a', // selector for Share to a Group
        groupName: 'input[placeholder="Group name"]', // group name input
        postButton: 'button._2g61.selected._42ft', // post button
        includeOrig: 'div > div > div._3-9a '+
        '> label > input[type=checkbox]', // include original post option
        shareDialog: 'div._59s7 > div._4t2a',
        actorSelect: 'span[data-testid="actor-selector"] a > span._55pe',
        voiceSelect: 'div[data-testid="voice-selector-dropdown"] input._5r_g',
        targetVoice: 'div[data-testid="voice-selector-dropdown"]'+
                    ' div[role="menuitem"]',

      },
    };
    if (!opts.browser) {
      throw new Error('Please pass a browser instance!');
    }
    this.#browser = opts.browser;
    this.origin = opts.origin;
    this.headless = typeof opts.headless === 'undefined' ? true : opts.headless;
    this.pathPrefix = typeof opts.pathPrefix !== 'undefined' ?
                      opts.pathPrefix : '/var/node/uitopost';
    const baseDir = this.pathPrefix;
    this.#auth = require(`${baseDir}/credentials.js`).facebook;
    const PAGE = targetPage;
    const configs = fs.readdirSync(`${baseDir}/pageData`);
    /* const confStr = configs.map((e) => {
      return e.replace(/\.js$/i, '');
    }).join(', ');*/
    if (configs.includes(`${PAGE}.js`)) {
      const conf = require(`${baseDir}/pageData/${PAGE}.js`);
      this.targetPage = conf.facebook;
      if(conf.friends && conf.friends.length > 0) {
        this.friendSharer = new FriendSharer(opts);
      }
    } else {
      throw new Error(`Target page ${PAGE} not found.`);
    }
  }
  /**
  * get page
  */
  async openPage() {
    const page = await this.#browser.newPage();
    await this.origin.evadeHeadlessDetection(page);
    page.setViewport({width: 1200, height: 800, deviceScaleFactor: 1});
    this.page = page;
    try {
      await this.login();
    } catch (e) { // already logged in
      console.log('already logged in');
    } finally {
      // oof
    }
  }
  /**
   * run puppeteer!
   * @param {number} postID - id of post to share
   */
  async run(postID) {
    await this.openPage();
    if(this.friendSharer) {
      await this.friendSharer.run(postID);
    }
    await this.getPost(postID, true);
  };
  /**
   * Coordinates for screenshot
   * @typedef {Object} Coordinates
   * @property {int} x - left edge of element
   * @property (int) y - top edge of element
   * @property {number} width - width of element
   * @property {number} height - height of element
   */

  /**
   * Options for screenshot
   * @typedef {Object} ScreenshotOpts
   * @property {string=} filePrefix - prefix for filename
   * @property {string=} filePath - path to save file
   * @property {Coordinates=} coords - optional {@link Coordinates} to target
   */


  /**
   * check if object is empty
   * @param {Object} obj - object to check
   * @return {boolean} true if object is empty
   */
  isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }

  /**
   * login to facebook
   * @param {Page} page - active puppeteer page
   */
  async login() {
    page = this.page;
    try {
      await page.waitForSelector(this.sel.login.submit, {timeout: 2000});
    } catch (e) {
      throw e;
    }
    // navigate to fb
    await page.goto('https://facebook.com', {waitUntil: 'domcontentloaded'});
    console.log('attempting fb login');
    // focus username field and type it
    console.log('inputting username');
    await page.focus(this.sel.login.user);
    await page.keyboard.type(this.#auth.username);

    // password field
    console.log('inputting password');
    await page.focus(this.sel.login.pass);
    await page.keyboard.type(this.#auth.password);

    // click login
    console.log('submitting login form');
    await page.click(this.sel.login.submit);
    await page.waitForNavigation();
    console.log('Logged In!');
  }

  /**
   * get posts from post id
   * @param {number} postID - id of post on facebook
   * @param {boolean=} schedule - whether or not to schedule share(s)
   */
  async getPost(postID, schedule) {
    const page = this.page;
    console.log(`[facebook::${this.usr}] getting post ${postID}`);
    const slug = this.targetPage.slug;
    const url = `https://facebook.com/${slug}/posts/${postID}`;
    const HOUR = 1000*60*60;
    await page.goto(url, {waitUntil: 'domcontentloaded'});
    if (typeof schedule !== 'undefined' && schedule) {
      for (const idx in this.targetPage.groups) {
        const group = this.targetPage.groups[idx];
        const OFFSET = HOUR*idx;
        console.log(`[facebook::${this.usr}] Scheduling ${postID} share to `+
                    `${group} with time offset: ${OFFSET}`);
        this.#timeouts.push({
          group,
          timeout: setTimeout(async (postID, group) => {
            await this.sharePost(postID, group);
          }, OFFSET, postID, group),
        });
        // await sharePost(page, group);
      }
      await page.goto('about:blank');
      // await page.close();
      // page = null;
    }
  }

  /**
   * share post to specific group
   * @param {number} postID - ID of target post
   * @param {string} group - name of group to share to
   */
  async sharePost(postID, group) {
    console.log(`[facebook::${this.usr}] in sharePost(${postID}, "${group}")`);
    if (!this.page) {
      await this.openPage();
    }
    await this.getPost(postID);
    const page = this.page;
    try {
      console.log(`[facebook::${this.usr}] clicking share link`);
      await page.waitForSelector(this.sel.post.share, {timeout: 2000});
      await page.click(this.sel.post.share);
      await page.waitForSelector(this.sel.post.shareToGroup, {timeout: 5000});
      console.log(`[facebook::${this.usr}] clicking share to group`);
      await page.click(this.sel.post.shareToGroup);
      await page.waitForSelector(this.sel.post.groupName, {timeout: 5000});
      const isSelf = await page.evaluate((sel) => {
        const actor = document.querySelector(sel.actorSelect);
        return actor.innerText.toLowerCase() === `gary warman`;
      }, this.sel.post);
      if (isSelf) {
        try {
          console.log(`[facebook::${this.usr}] `+
            `changing post as to proper group`);
          await page.click(this.sel.post.actorSelect);
          await page.waitForSelector(
              this.sel.post.voiceSelect, {timeout: 3000},
          );
          await page.focus(this.sel.post.voiceSelect);
          await page.keyboard.type(this.targetPage.actorQuery);
          if (await page.evaluate(
              (sel, q) => {
                const target = document.querySelector(sel);
                const ret = target.innerText.match(new RegExp(q, `i`));
                return ret;
              },
              this.sel.post.targetVoice, this.targetPage.actorQuery,
          )) {
            console.log(`[facebook::${this.usr}] `+
              `selecting page as target voice`);
            await page.click(this.sel.post.targetVoice);
          }
        } catch (e) {
          console.log(`[facebook::${this.usr}] unable to change post voice.  error: ${e}`);
          return;
        }
      }
      try {
        console.log(`[facebook::${this.usr}] clicking include original`);
        await page.click(this.sel.post.includeOrig);
      } catch(e) {
        console.log(`[facebook::${this.usr}] no original to include. skipping`);
      }
      console.log(`[facebook::${this.usr}] focusing on group name`);
      await page.focus(this.sel.post.groupName);
      await page.keyboard.type(group);
      console.log(`[facebook::${this.usr}] typed group name`);
      await page.waitFor(1000);
      await page.keyboard.press(`ArrowDown`);
      console.log(`[facebook::${this.usr}] arrow down!`);
      await page.waitFor(1000);
      await page.keyboard.press(`Enter`);
      console.log(`[facebook::${this.usr}] enter pressed`);
      await page.waitFor(500);
      const postButton = await page.$(this.sel.post.postButton);
      console.log(`[facebook::${this.usr}] clickin post butan!`);
      await postButton.click();
      let success;
      try {
        await page.waitForSelector('._4t2a ._pig', {timeout: 5000});
        const res = await page.evaluate(() => {
          return document.querySelector('._4t2a ._pig').innerText;
        })
        success = res.match(/success/i);
      } catch (e) {
      }
      if(success) {
        console.log(`[facebook::${this.usr}] Post ${postID} `+
            `shared successfully to ${group}`);
      } else {
        console.log(`[facebook::${this.usr}] Post ${postID} `+
            `unable to share to ${group}`);
      }
    } catch (e) {
      console.log(`[facebook::${this.usr}] error sharing post to`, group, `: `);
      console.error(e);
    } finally {
      await page.goto('about:blank');
      // await page.close();
      // page = null;
      // await this.closeBrowser();
    }
  }
}

module.exports.FacebookSharer = FacebookSharer;
